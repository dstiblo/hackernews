<?php

require_once __DIR__.'/vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
require_once 'src/services/GuzzleService.php';


define('NUMBER_OF_NEWS_ON_PAGE', 10);
$app = require __DIR__.'/src/app.php';
require __DIR__.'/config/prod.php';
require __DIR__.'/src/controllers.php';

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

$app->register(new GuzzleServiceProvider(), array(
    'guzzle.timeout' => 3.14,
    'guzzle.request_options' => ['auth' => ['admin', 'admin']]
));

$request = $app['guzzle']->get('https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty');

$newsIdList = explode(', ', $request->getBody()->getContents());
$newsIdList[0] = trim(str_replace('[', '', $newsIdList[0]));
$lastItemIndex = count($newsIdList) - 1;
$newsIdList[$lastItemIndex] = trim(str_replace(']', '', $newsIdList[$lastItemIndex]));

for($i = 0; $i <= NUMBER_OF_NEWS_ON_PAGE; $i++) {
    $request = $app['guzzle']->get('https://hacker-news.firebaseio.com/v0/item/' . $newsIdList[$i] . '.json?print=pretty');
    $newsInfoList[] = json_decode($request->getBody()->getContents());
}
$app["twig"]->addGlobal("newsInfoList", $newsInfoList);
$app->run();
